package ro.test.sonar.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ro.test.sonar.bean.Champion;
import ro.test.sonar.bean.Game;
import ro.test.sonar.utils.JacksonConvertor;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SonarTestApplication {

    public static void main(String[] args) throws JsonProcessingException {
        Game game1 = new Game("League of Legends");
        Champion adc1 = new Champion("Caitlyn", "ADC");
        game1.getChampions().add(adc1);

        List<Champion> championListForImport = new ArrayList<>();
        Champion support1 = new Champion("Leona", "Support");
        Champion support2 = new Champion("Jhin", "ADC");
        Champion support3 = new Champion("Lulu", "Support");
        Champion support4 = new Champion("Janna", "Support");
        Champion support5 = new Champion("Jinx", "ADC");
        championListForImport.add(support1);
        championListForImport.add(support2);
        championListForImport.add(support3);
        championListForImport.add(support4);
        championListForImport.add(support5);

        JacksonConvertor jacksonConvertor = new JacksonConvertor();
        game1.addChampionsWithSpecificRoles("Support", championListForImport);
        System.out.println("Locuri: " + jacksonConvertor.toJson(game1));

        for (Champion champ : championListForImport) {
            List<Champion> newListChamp = new ArrayList<>();
            newListChamp.add(champ);
        }

        SpringApplication.run(SonarTestApplication.class, args);
    }
}
