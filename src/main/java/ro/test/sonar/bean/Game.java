package ro.test.sonar.bean;

import java.util.ArrayList;
import java.util.List;

public class Game {

    public String name;
    public List<Champion> champions;

    public Game(String name) {
        this.name = name;
        this.champions = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Champion> getChampions() {
        return champions;
    }

    public void setChampions(List<Champion> champions) {
        this.champions = champions;
    }

    public void addChampionsWithSpecificRoles(String role, List<Champion> championsList){
        for (Champion champion : championsList) {
            if(champion.getRole().equals(role)){
                this.getChampions().add(champion);
            }
        }
    }
}
